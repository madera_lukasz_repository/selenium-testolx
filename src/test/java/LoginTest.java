import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest{

    @Test
    public void shouldIdLogin(){
        loginPage.openTestOLX();
        loginPage.fillEmail("mail");
        loginPage.fillPassword("password");
        loginPage.clickLoginButton();

        Assert.assertTrue(dashboardPage.isLogoutButtonPresent());

    }


    @Test (enabled = false)
    public void shouldNotLoginWithoutLoginAndPasswordShouldAppearErrorMessage() {
        loginPage.openTestOLX();
        loginPage.clickLoginButton();
        Assert.assertTrue(dashboardPage.loginErrorPresent());
    }

}

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;

import javax.naming.directory.SearchResult;
import java.io.File;
import java.io.IOException;
import java.sql.Time;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;


public class BaseTest {

    WebDriver driver; //w zwiazku z utworzeniem beforetest nie mozemy inicjalizowac driver przez podaniem sciezki do niego
    LoginPage loginPage; //deklarujemy  xeby isntiało do wszystkich pol
    DashboardPage dashboardPage;
    ClassifiedsPage classifiedsPage;
    SearchResultsPage searchresultsPage;




    @BeforeTest //tylko te ktore są oznaczone jako test //method przed kazda metoda w klasie
    public void before() {
        //System.setProperty("webdriver.gecko.driver","C:\\selendriver\\geckodriver.exe");
        System.setProperty("webdriver.chrome.driver","C:\\selendriver\\chromedriver.exe");
        //driver = new FirefoxDriver(); //inicjalizacja drivera firefox
        driver = new ChromeDriver(); //inicjalizacja drivera chrome
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        loginPage = new LoginPage(driver);
        dashboardPage = new DashboardPage(driver);
        classifiedsPage = new ClassifiedsPage(driver);
        searchresultsPage = new SearchResultsPage(driver);

    }

    @AfterMethod
    public void tearDownMethod(ITestResult result) throws IOException {
        //jesli failt rob zrzut
        if (result.isSuccess()) {
            File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenshot, new File(
                    BaseTest.screenshotsPath + "screenshot_"
                            + result.getMethod().getMethodName()
                            + "_"
                            + new Date().getTime()
                            + ".png"));
        }
    }

    protected static String screenshotsPath = "C:\\Users\\lukaszm\\Desktop\\screenshot\\";
}

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage {

//    WebDriver driver; wywalam

    @FindBy(css = "#userEmail")
    WebElement emailInPut;

    @FindBy(css = "#userPass")
    WebElement passwordInput;

    @FindBy(css = "#se_userLogin")
    WebElement buttonLoginInput;


    public LoginPage(WebDriver driver) {
        super(driver); //daje super wiec pozostałe elementy są już zbędne
//        this.driver = driver; //odwolanie się do instancji samego siebie
//        PageFactory.initElements(driver,this);
    }

    public void openTestOLX() {
        driver.get("https://www.olx.pl/konto/?ref%5B0%5D%5Baction%5D=myaccount&ref%5B0%5D%5Bmethod%5D=index#login");
    }

    public void fillEmail(String email) { //string email - to parametr metody
        //driver.findElement(By.cssSelector("#email")).sendKeys(email); //by to jest jakis obiekt lub klasa
        emailInPut.sendKeys(email); //uzycie pod findby - aby nie było koniecznosci wpisywania wyszukiwania selectrora...
    }

    public void fillPassword(String password) {
        //driver.findElement(By.cssSelector("#password")).sendKeys(password);
        passwordInput.sendKeys(password);
    }

    public void clickLoginButton() {
        //driver.findElement(By.cssSelector("#login")).click();
        buttonLoginInput.click();
    }


}

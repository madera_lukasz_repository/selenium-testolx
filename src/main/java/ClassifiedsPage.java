import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ClassifiedsPage extends BasePage {

    @FindBy(css = "#headerLogo")
    WebElement clickLogo;

    @FindBy(css = "#searchmain-container > div > div.maincategories > div:nth-child(1) > div:nth-child(1) > div > a > span:nth-child(1)")
    WebElement clickkMotoryzacja;

    @FindBy(css = "#bottom5 > ul > li:nth-child(1) > a > span > span")
    WebElement clickOsobowe;

    @FindBy(css = "#cityField")
    WebElement cityInput;

    @FindBy(css = "#autosuggest-geo-ul > li:nth-child(1) > a")
    WebElement clickCity;

    @FindBy(xpath = "//*[@id=\"param_price\"]/div/div[2]/a")
    WebElement clickCenaDoInput;

    @FindBy(xpath = "//*[@id=\"param_price\"]/div/div[2]/ul/li[4]/a")
    WebElement cenaDoInput;

    @FindBy(xpath = "//*[@id=\"param_year\"]/div/div[1]/a")
    WebElement clickRokProdOd;

    @FindBy(xpath = "//*[@id=\"param_year\"]/div/div[1]/ul/li[4]/a")
    WebElement rokProdOdInput;

    @FindBy(xpath = "//*[@id=\"param_milage\"]/div/div[2]/a")
    WebElement clickPrzebiegDo;

    @FindBy(xpath = "//*[@id=\"param_milage\"]/div/div[2]/ul/li[9]/a")
    WebElement PrzebiegDoInput;


    @FindBy(css = "#tabs-container > div > ul > li:nth-child(2) > a") //xpath = "//*[@id=\"tabs-container\"]/div/ul/li[2]")
            WebElement clickPrywatne;

    @FindBy(xpath = "//*[@id=\"param_transmission\"]/div/a/span[1]")
    WebElement clickSkrzyniaBiegow;

    @FindBy(xpath = "//*[@id=\"param_transmission\"]/div/ul/li[2]/label[2]")
    WebElement SkrzyniaBiegowInput;


    // @FindBy(xpath = "//*[@id=\"param_petrol\"]/div/a/span[1]")
    @FindBy(xpath = "//*[@id=\"param_petrol\"]")
    WebElement clickPaliwo;

    @FindBy(xpath = "//*[@id=\"param_petrol\"]/div/ul/li[2]/label[1]")
    WebElement clickBenzyna;

    @FindBy(xpath = "//*[@id=\"param_petrol\"]/div/ul/li[4]/label[1]")
    WebElement clickLpg;


//    @FindBy(xpath = "//*[@id=\"param_petrol\"]/div/ul/li[2]/label[2]")
//    WebElement Paliwo1Input;

    @FindBy(css = "#search-submit")
    WebElement clickSearch;

//    @FindBy(xpath = "//*[@id=\"param_petrol\"]/div/ul/li[4]/label[2]")
//    WebElement Paliwo2Input;

    public ClassifiedsPage(WebDriver driver) {
        super(driver);
    }

    public void clickLogoButton() {
        clickLogo.click();
    }

    public void fillCity(String name) {
        cityInput.sendKeys(name);
        //cityInput.click();
    }

    public void clickCityButton() {
        clickCity.click();
    }

    public void clickSearchButton() {
        clickSearch.click();
    }

    public void clickkMotoryzacjaButton() {
        clickkMotoryzacja.click();
    }

    public void clickOsoboweButton() {
        clickOsobowe.click();
    }

    public void ClickCenaDoInput() {
        clickCenaDoInput.click();
    }

    public void clickCenaDo() {
        cenaDoInput.click();
    }

    public void ClickRokProdOd() {
        clickRokProdOd.click();
    }

    public void clickRokProdOd() {
        rokProdOdInput.click();
    }

    public void clickPrywatneButton() {
        clickPrywatne.click();
    }

    public void ClickPrzebiegDo() {
        clickPrzebiegDo.click();
    }

    public void clickPrzebiegDo() {
        PrzebiegDoInput.click();
    }

    public void ClickSkrzyniaBiegow() {
        clickSkrzyniaBiegow.click();
    }

    public void clickSkrzyniaBiegow() {
        SkrzyniaBiegowInput.click();
    }

    public void ClickPaliwo() {
        clickPaliwo.click();
        clickBenzyna.click();
        clickLpg.click();
    }


//    public void fillPaliwo() {
//        //Paliwo1Input.click();
//        //  Paliwo2Input.click();
//    }

}

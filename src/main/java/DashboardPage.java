import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class DashboardPage extends BasePage {

    //bedzie nam obslugiwać stronę głowna

    @FindBy(css = ".header_logout a")
    WebElement logOutButton;


    public DashboardPage(WebDriver driver) {
        super(driver);
    }

 /*   public boolean isLogoutButtonVisible() {
        return logOutButton.isDisplayed();
    }*/

    //metoda do weryfikacji poprawnosc logowania
    public boolean isLogoutButtonPresent() {
        List<WebElement> tmp = driver.findElements(By.cssSelector("#login-box-logout"));
        return tmp.size() != 0;
    }


    //metoda blednego logowania
    public boolean loginErrorPresent() {
        List<WebElement> tmp = driver.findElements(By.cssSelector("#se_emailError > div"));
        //System.out.println(tmp.size());
        return tmp.size() > 0;
    }

    /*//metoda validacji błędu
    public boolean validErrorPresent() {
        List<WebElement> tmp = driver.findElements(By.cssSelector(".error_msg"));
        System.out.println(tmp.size());
        return tmp.size() !=0;
    }*/

}

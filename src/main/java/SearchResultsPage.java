import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;


public class SearchResultsPage extends BasePage {

    @FindBy(id = "offers_table")
    WebElement findListOffers;

    @FindBy(xpath = "//table[@id='offers_table']/tbody/tr[2]/td/table/tbody/tr/td[2]/div/h3/a/strong")
    WebElement linki;




    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }


        public void listOffers() {

            List<WebElement> rows = findListOffers.findElements(By.cssSelector("#offers_table > tbody "));
            //wskazuje tablice
            System.out.println("rozpoczecie petli");
            for (WebElement row : rows) {
              //buduję zmienną for
                    System.out.println(row.getText() + "\t");
                List<WebElement> linki = findListOffers.findElements(By.tagName("a"));
                    for (WebElement link:linki) {
                        System.out.println(link.getAttribute("href"));
                    }

                }
                System.out.println("zakonczenie petli");

            }


    public void clickLinki() {
        linki.click();
    }


}
